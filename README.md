# ECO 586

## Exercise sessions

### José MORAN

This page contains the subjects and data you'll be using for the exercise session of the *phenomenology and modeling of financial markets* course. 

Throughout the course, we will be working with Python, using Jupyter Notebooks (or Jupyter lab) to work. If you wish to install Python on your own computer, I highly recommend the [getting started guide](https://python.quantecon.org/getting_started.html)  from the QuantEcon site by Sargent and Stachurski. Do use Anaconda to manage your environment. 

The same guide helps you getting acquainted with Jupyter notebooks, which are now standard across industry (tech and finance for instance) and academia.



Support for Python 2.7 is obsolete as of Jan. 2020, we will therefore be using **Python 3** and we will be working with the following packages : 

* `numpy`: a very common library used for scientific computation. Version >= 1.17
*  `scipy`: another library used for scientific computation, we will mostly use the `scipy.stats` module. Version >= 1.3
* `pandas`: **the** library for working with data. Version >= 0.25
* `networkx`: an extremely useful library to work with graphs. Version >= 2.4
* `matplotlib`: a very basic library to do plots in python (but you are free to use others, e.g. `plotly` if you wish). Version >= 2.2.2



## Guides 

If your knowledge of Python is non-existent, work your way through the essential guides on the QuantEcon site, such as the [Python essentials](https://python.quantecon.org/python_essentials.html), teaching you the very basics of Python programming. 

We will also be using Object Oriented Programming for simulations, which you can learn about on the [first](https://python.quantecon.org/oop_intro.html) and [second](https://python.quantecon.org/python_oop.html) guides on the same site.

I have also written small guides on `numpy` and `pandas` that concentrate on the tools needed for the course. You can find them on the `/Cheatsheets/` subdirectory of this `git` repo, both in the `.ipynb` format that you can run as a Jupyter notebook, or you can just download them as an `.html`file. 

#### Downloading files

To download files from this repo, you can either clone the directory if you are familiar with how `git` works, or go to the file on Bitbucket, click on the `...` on the top right corner of the source code and clicking on `Open raw` and then saving the file.